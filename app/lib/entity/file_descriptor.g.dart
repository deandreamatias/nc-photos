// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'file_descriptor.dart';

// **************************************************************************
// ToStringGenerator
// **************************************************************************

extension _$FileDescriptorToString on FileDescriptor {
  String _$toString() {
    // ignore: unnecessary_string_interpolations
    return "FileDescriptor {fdPath: $fdPath, fdId: $fdId, fdMime: $fdMime, fdIsArchived: $fdIsArchived, fdIsFavorite: $fdIsFavorite, fdDateTime: $fdDateTime}";
  }
}
