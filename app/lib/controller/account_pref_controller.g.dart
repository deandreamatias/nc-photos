// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_pref_controller.dart';

// **************************************************************************
// NpLogGenerator
// **************************************************************************

extension _$AccountPrefControllerNpLog on AccountPrefController {
  // ignore: unused_element
  Logger get _log => log;

  static final log =
      Logger("controller.account_pref_controller.AccountPrefController");
}
